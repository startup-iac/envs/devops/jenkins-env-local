data "terraform_remote_state" "tls" {
  backend = "local"

  config = {
    path = "../03-tls-certificates/terraform.tfstate"
  }
}

locals {

  iam_dns_name                = "iam.${local.instance_name}.${local.dns_name}"
  monitoring_dns_name         = "mon.${local.instance_name}.${local.dns_name}"
  source_dns_name             = "source.${local.instance_name}.${local.dns_name}"
  cicd_dns_name               = "cicd.${local.instance_name}.${local.dns_name}"
  sast_dns_name               = "sast.${local.instance_name}.${local.dns_name}"
  repository_dns_name         = "artifactory.${local.instance_name}.${local.dns_name}"
  cluster_issuer_name         = data.terraform_remote_state.tls.outputs.cluster_issuer_name
  subject_organizations       = data.terraform_remote_state.tls.outputs.cluster_subject_organizations
  subject_organizationalunits = data.terraform_remote_state.tls.outputs.cluster_subject_organizationalunits
  iam_client_secret           = "Vwe39UMOIL9Qr6MKAcTw0MB2QANlZ3lP"
}


resource "kubernetes_namespace" "devops" {
  metadata {
    name = var.namespace
  }
}

data "template_file" "init_realms" {
  template = "${file("${path.module}/devops-realm.json")}"
  vars = {
    CICD_DNS_NAME     = local.cicd_dns_name
    SAST_DNS_NAME     = local.sast_dns_name
    IAM_CLIENT_SECRET = local.iam_client_secret
  }
}

# module "database" {
#   source = "git::https://gitlab.com/startup-iac/terraform/cluster-services/commons/postgresql-ha.git"

#   namespace                   = var.namespace
#   dns_name                    = ""
#   cluster_issuer_name         = local.cluster_issuer_name
#   subject_organizations       = local.subject_organizations
#   subject_organizationalunits = local.subject_organizationalunits
  
# }


module "iam" {
  source = "git::https://gitlab.com/startup-iac/terraform/cluster-services/commons/keycloak.git"

  namespace                   = var.namespace
  dns_name                    = local.iam_dns_name
  cluster_issuer_name         = local.cluster_issuer_name
  subject_organizations       = local.subject_organizations
  subject_organizationalunits = local.subject_organizationalunits
  init_realms                 = data.template_file.init_realms.rendered
}


module "monitoring" {
  source = "git::https://gitlab.com/startup-iac/terraform/cluster-services/commons/monitoring.git"

  namespace                   = var.namespace
  dns_name                    = local.monitoring_dns_name
  cluster_issuer_name         = local.cluster_issuer_name
  subject_organizations       = local.subject_organizations
  subject_organizationalunits = local.subject_organizationalunits
  iam_dns_name                = local.iam_dns_name
  iam_client_secret           = local.iam_client_secret

  depends_on = [ module.iam ]
}


# module "source" {
#   source = "git::https://gitlab.com/startup-iac/terraform/cluster-services/devops/gitea.git"

#   namespace                   = var.namespace
#   imagetag                    = ""
#   dns_name                    = local.source_dns_name
#   cluster_issuer_name         = local.cluster_issuer_name
#   subject_organizations       = local.subject_organizations
#   subject_organizationalunits = local.subject_organizationalunits
# }

# module "sast" {
#   source = "git::https://gitlab.com/startup-iac/terraform/cluster-services/devops/sonarqube.git"

#   namespace                   = var.namespace
#   dns_name                    = local.sast_dns_name
#   cluster_issuer_name         = local.cluster_issuer_name
#   subject_organizations       = local.subject_organizations
#   subject_organizationalunits = local.subject_organizationalunits
#   iam_dns_name                = local.iam_dns_name
#   iam_client_secret           = local.iam_client_secret

#   depends_on = [ module.iam ]
# }

# module "artifactory" {
#   source = "git::https://gitlab.com/startup-iac/terraform/cluster-services/devops/nexus-repository.git"

#   namespace                   = var.namespace
#   dns_name                    = local.repository_dns_name
#   cluster_issuer_name         = local.cluster_issuer_name
#   subject_organizations       = local.subject_organizations
#   subject_organizationalunits = local.subject_organizationalunits
#   iam_dns_name                = local.iam_dns_name
#   iam_client_secret           = local.iam_client_secret

#   depends_on = [ module.iam ]
# }

# module "cicd" {
#   source = "git::https://gitlab.com/startup-iac/terraform/cluster-services/devops/jenkins.git"

#   namespace                   = var.namespace
#   dns_name                    = local.cicd_dns_name
#   cluster_issuer_name         = local.cluster_issuer_name
#   subject_organizations       = local.subject_organizations
#   subject_organizationalunits = local.subject_organizationalunits
#   iam_dns_name                = local.iam_dns_name
#   iam_client_secret           = local.iam_client_secret

#   depends_on = [ module.iam, module.sast ]
# }



# kubectl -n citadel exec <PGPOOL POD> -- /bin/bash
# touch /opt/bitnami/pgpool/conf/pgpool.conf
# pg_md5 -m --config-file="/opt/bitnami/pgpool/conf/pgpool.conf" -u "USERNAME" "PASSWORD"
# # extra measures
# echo "host all all 0.0.0.0/0 md5"
# echo "host all all 0.0.0.0/0 trust"