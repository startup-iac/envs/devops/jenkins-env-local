#!/bin/bash

# Add CoreDNS:
# rewrite name iam.local.local keycloak.devops-ns.svc.cluster.local

POD_NAME="curl-pod"

kubectl run $POD_NAME --image=curlimages/curl --restart=Never -- sleep infinity

while true; do
  # Obtener el estado del pod
  STATUS=$(kubectl get pod $POD_NAME --no-headers -o custom-columns=":status.phase")
  READY=$(kubectl get pod $POD_NAME --no-headers -o custom-columns=":status.conditions[?(@.type=='Ready')].status")

  # Verificar si el estado es Running y si está listo
  if [[ "$STATUS" == "Running" && "$READY" == "True" ]]; then
    echo "Pod $POD_NAME está listo."
    kubectl exec $POD_NAME -- nslookup keycloak.devops-ns.svc.cluster.local
    kubectl exec $POD_NAME -- nslookup iam.local.local
    break
  else
    echo "Esperando a que el pod $POD_NAME esté listo..."
  fi

  # Esperar un poco antes de volver a verificar
  sleep 5
done

kubectl delete pod $POD_NAME

