keytool -import -alias mycert -keystore $JAVA_HOME/lib/security/cacerts -file /etc/ssl/certs/mycert/my-certificate.crt -storepass changeit -noprompt


export KEYSTORE_PATH="$JAVA_HOME/lib/security/cacerts"
keytool -import -noprompt -trustcacerts -alias key -file "/var/jenkins_home/key.pem" -keystore "${KEYSTORE_PATH}" -storepass changeit

keytool -import -noprompt -trustcacerts -cacerts -alias iam -file "/var/jenkins_home/iam.pem" -storepass changeit 
