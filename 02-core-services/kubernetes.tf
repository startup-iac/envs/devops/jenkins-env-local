data "terraform_remote_state" "cluster" {
  backend = "local"

  config = {
    path = "../01-cluster/terraform.tfstate"
  }
}

locals {
  instance_name = data.terraform_remote_state.cluster.outputs.cluster_instance_name
  dns_name      = data.terraform_remote_state.cluster.outputs.cluster_dns_name
  environments  = data.terraform_remote_state.cluster.outputs.cluster_environments
}

provider "helm" {
  kubernetes {
    config_path = "~/.kube/config"
  }
}

provider "kubernetes" {
  config_path = "~/.kube/config"
}

