module "cluster_core_services" {
  source = "git::https://gitlab.com/startup-iac/terraform/cluster-services/commons/core-services.git"

  cluster_type          = "local"
  cluster_instance_name = local.instance_name
  cluster_environments  = local.environments
  cluster_dns_name      = local.dns_name
  cluster_ingress_ip    = ""
}