module "tls_certificates" {
  source = "git::https://gitlab.com/startup-iac/terraform/cluster-services/commons/tls-certificates.git"

  cluster_instance_name               = local.instance_name
  cluster_environments                = local.environments
  cluster_dns_name                    = local.dns_name
  cluster_issuer_email                = var.cluster_issuer_email
  cluster_issuer_server               = var.cluster_issuer_server
  cluster_subject_organizations       = var.cluster_subject_organizations
  cluster_subject_organizationalunits = var.cluster_subject_organizationalunits

}

