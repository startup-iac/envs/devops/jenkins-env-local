
# email for certificate domain
cluster_issuer_email = "anayaj0511@hotmail.com"
# Dev Certificates
cluster_issuer_server = "https://acme-staging-v02.api.letsencrypt.org/directory"
# Prod Certificates 
#cluster_issuer_server = "https://acme-v02.api.letsencrypt.org/directory" 
cluster_subject_organizations       = "startup-framework"
cluster_subject_organizationalunits = "devops"