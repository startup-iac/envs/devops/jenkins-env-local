output "cluster_issuer_name" {
  value = module.tls_certificates.cluster_issuer_name
}

output "cluster_subject_organizations" {
  value = module.tls_certificates.cluster_subject_organizations
}

output "cluster_subject_organizationalunits" {
  value = module.tls_certificates.cluster_subject_organizationalunits
}