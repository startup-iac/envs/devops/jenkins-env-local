variable "cluster_issuer_server" {
  type = string
}

variable "cluster_issuer_email" {
  type = string
}

variable "cluster_subject_organizations" {
  type = string
}

variable "cluster_subject_organizationalunits" {
  type = string
}