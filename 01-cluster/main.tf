module "cluster_environments" {
  for_each = toset(var.cluster_environments)
  source   = "git::https://gitlab.com/startup-iac/terraform/cluster-services/commons/environment.git"

  environment_name = "${var.cluster_instance_name}-${each.key}"
}
