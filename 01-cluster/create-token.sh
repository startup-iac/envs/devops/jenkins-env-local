NAMESPACE=$(terraform output -raw dashboard_namespace)
SERVICE_ACCOUNT=$(terraform output -raw dashboard_user)

kubectl -n $NAMESPACE create token $SERVICE_ACCOUNT --duration=72h