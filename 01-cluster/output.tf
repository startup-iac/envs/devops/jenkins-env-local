output "cluster_instance_name" {
  value       = var.cluster_instance_name
  description = "Cluster instance name"
}

output "cluster_dns_name" {
  value       = var.cluster_dns_name
  description = "DNS cluster name"
}

output "cluster_environments" {
  value       = var.cluster_environments
  description = "List of Environment Names."
}

output "cluster_namespaces" {
  value = [for item in module.cluster_environments : item.namespace]
}

output "dashboard" {
  value = "To access UI Dashborard execute: kubectl proxy"
}

output "dashboard_namespace" {
  value = local.namespace
}

output "dashboard_user" {
  value = local.service_account
}

output "dashboard_ui" {
  value = "http://localhost:8001/api/v1/namespaces/${local.namespace}/services/https:main-kubernetes-dashboard:https/proxy/"
}

output "create_token_script" {
  value = "create-token.sh"
}