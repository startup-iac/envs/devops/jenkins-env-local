# For more info view:
# https://artifacthub.io/packages/helm/k8s-dashboard/kubernetes-dashboard

locals {
  service_account = "admin-user"
  namespace       = "dashboard-ns"
  module_path     = abspath(path.module)
  tokenfile       = "${local.module_path}/${local.service_account}.token"
}

resource "helm_release" "kubernetes_dashboard" {
  name             = "main"
  namespace        = local.namespace
  create_namespace = true
  version          = "6.0.8"

  repository = "https://kubernetes.github.io/dashboard/"
  chart      = "kubernetes-dashboard"

  #values = [
  #  "${file("${path.module}/dashboard-values.yaml")}"
  #]

}

resource "kubernetes_manifest" "service_account" {
  manifest = {
    "apiVersion" = "v1"
    "kind"       = "ServiceAccount"
    "metadata" = {
      "name"      = local.service_account
      "namespace" = local.namespace
    }
  }
  depends_on = [
    helm_release.kubernetes_dashboard
  ]
}

resource "kubernetes_manifest" "cluster_role_binding" {
  manifest = {
    "apiVersion" = "rbac.authorization.k8s.io/v1"
    "kind"       = "ClusterRoleBinding"
    "metadata" = {
      "name" = local.service_account
      #"namespace" = local.namespace
    }
    "roleRef" = {
      "apiGroup" = "rbac.authorization.k8s.io"
      "kind"     = "ClusterRole"
      "name"     = "cluster-admin"
    }
    "subjects" = [
      {
        "kind"      = "ServiceAccount"
        "name"      = local.service_account
        "namespace" = local.namespace
      }
    ]
  }

  depends_on = [
    kubernetes_manifest.service_account
  ]
}
